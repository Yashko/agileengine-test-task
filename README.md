# AgileEngine Test Task

## Usage
1. Close repository, run `npm install`
2. `node pinkerton.js <input_origin_file_path> <input_other_sample_file_path>`

## Tips
- You can analyze default samples via `npm run case1`, `npm run case2` etc
- Use `--element-id=whatever` to specify id of the original element ("make-everything-ok-button" by default)
- Use `--make-notes` to see detailed output
- Executables for different platforms can be found under *bin/* folder

## Output for sample pages:
1. `HTML > BODY > DIV[1] > DIV[1] > DIV[3] > DIV[1] > DIV[1] > DIV[2] > A[2]`
2. `HTML > BODY > DIV[1] > DIV[1] > DIV[3] > DIV[1] > DIV[1] > DIV[2] > DIV[1] > A[1]`
3. `HTML > BODY > DIV[1] > DIV[1] > DIV[3] > DIV[1] > DIV[1] > DIV[3] > A[1]`
4. `HTML > BODY > DIV[1] > DIV[1] > DIV[3] > DIV[1] > DIV[1] > DIV[3] > A[1]`

